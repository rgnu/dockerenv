ENVIRONMENTS+=whoami

dockerenv.whoami.deps: dockerenv.image.whoami.build dockerenv.common.proxy.up 

dockerenv.whoami.up: dockerenv.whoami.deps
dockerenv.whoami.%.up: dockerenv.whoami.deps
