ENVIRONMENTS+=image

.PRECIOUS: image/images/%
image/images/%: ;

dockerenv.image.base-ubuntu-14-04.build: \
	image/images/base-ubuntu-14.04/Dockerfile

dockerenv.image.base-ubuntu-18-04.build: \
	image/images/base-ubuntu-18-04/Dockerfile

dockerenv.image.base-alpine-3-9.build: \
	$(call rwildcard, $(CURDIR)/image/images/base-alpine-3-9, *)

dockerenv.image.mailcatcher.build: \
	dockerenv.image.base-ubuntu-14-04.build

dockerenv.image.redis.build: \
	dockerenv.image.base-ubuntu-14-04.build

dockerenv.image.java-7-openjdk.build: \
	dockerenv.image.base-ubuntu-14-04.build

dockerenv.image.zookeeper.build: \
	dockerenv.image.java-7-openjdk.build

dockerenv.image.kafka-0-10.build: \
	dockerenv.image.java-7-openjdk.build

dockerenv.image.graalvm.build: \
	dockerenv.image.base-ubuntu-14-04.build

dockerenv.image.mongo-3-2.build: \
	dockerenv.image.base-ubuntu-14-04.build

dockerenv.image.vertx-hello: \
	dockerenv.image.vertx-hello.build

dockerenv.image.metrics-pg.build: \
	$(call rwildcard, $(CURDIR)/image/images/metrics-pg, *.js *package.json *Dockerfile)

dockerenv.image.docker-logs.build: \
	$(call rwildcard, $(CURDIR)/image/images/docker-logs, *.js *package.json *Dockerfile)

dockerenv.image.mule-4.build: \
	$(call rwildcard, $(CURDIR)/image/images/mule-4, *)

dockerenv.image.node-6-runtime.build: \
	image/images/node-6-runtime/Dockerfile \
	dockerenv.image.base-ubuntu-14-04.build

dockerenv.image.node-6-build.build: \
	image/images/node-6-build/Dockerfile \
	dockerenv.image.node-6-runtime.build

dockerenv.image.whoami.build: \
	image/images/whoami/Dockerfile
