#!/bin/sh

exec /opt/mule/bin/mule \
    ${MULE_ANYPOINT_URI:+-M-Danypoint.platform.base_uri=$MULE_ANYPOINT_URI} \
    ${MULE_ANYPOINT_CLIENT_ID:+-M-Danypoint.platform.client_id=$MULE_ANYPOINT_CLIENT_ID} \
    ${MULE_ANYPOINT_CLIENT_SECRET:+-M-Danypoint.platform.client_secret=$MULE_ANYPOINT_CLIENT_SECRET} \
    ${PROXY_HOST:+-M-Danypoint.platform.proxy_host=$PROXY_HOST} \
    ${PROXY_PORT:+-M-Danypoint.platform.proxy_port=$PROXY_PORT} \
    ${PROXY_INSECURE:+-M-Danypoint.platform.enable_ssl_validation=false}
