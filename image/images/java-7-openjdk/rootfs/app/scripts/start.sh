#!/bin/bash

DIR=$(dirname $(dirname $0))
JAR=$(ls $DIR/*.jar 2>/dev/null)
CP=${JAR:+$DIR:$JAR}

[ -f /opt/newrelic/newrelic.yml ] \
&& [ -f /opt/newrelic/newrelic.jar ] \
&& JAVA_OPTS="${JAVA_OPTS} -javaagent:/opt/newrelic/newrelic.jar"

[ -f $DIR/config/log4j.properties ] \
&& JAVA_OPTS="${JAVA_OPTS} -Dlog4j.configuration=file:$DIR/config/log4j.properties"

[ -f $DIR/log4j.properties ] \
&& JAVA_OPTS="${JAVA_OPTS} -Dlog4j.configuration=file:$DIR/log4j.properties"

JAVA_OPTS="${JAVA_OPTS} -Xmx${JAVA_HEAP_MEMORY:-256m}"

echo "Start ${CP}" \
&& cd $DIR \
&& exec java ${JAVA_OPTS} -jar ${JAR}
