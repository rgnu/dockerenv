
eventMessageDebug() {
  [ $VERBOSE -ge 4 ] && eventMessage "[DEBUG] $1" || true
}

eventMessageInfo() {
  [ $VERBOSE -ge 3 ] && eventMessage "[INFO ] $1" || true
}

eventMessageWarning() {
  [ $VERBOSE -ge 2 ] && eventMessage "[WARN ] $1" || true
}

eventMessageError() {
  [ $VERBOSE -ge 1 ] && eventMessage "[ERROR] $1" || true
}

eventMessage() {
  local HANDLER=$(basename $0)
  logger -s -t "$EVENT[$HANDLER]" "$1"
}
