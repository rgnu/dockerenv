export PID=$$
export SERVICE=${SERVICE:-$(basename $PWD)}

exec 2> >(exec logger -t "$SERVICE[$PID]" -p daemon.error)
exec 1> >(exec logger -t "$SERVICE[$PID]" -p daemon.info)

echo "Starting" \
&& /etc/event/emit service.start.$SERVICE
