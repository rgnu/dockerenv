export PID=$$
export SERVICE=$(basename $PWD)

exec 2> >(exec logger -t "$SERVICE[$PID]" -p daemon.error)
exec 1> >(exec logger -t "$SERVICE[$PID]" -p daemon.info)
