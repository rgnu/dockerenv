CREATE TABLE metricseries (
  hash_id BIGINT UNIQUE NOT NULL,
  name VARCHAR(100) NOT NULL,
  tags JSONB
);
CREATE INDEX idxgintags ON metricseries USING gin ((tags));


CREATE TABLE metricvalues (
  hash_id BIGINT NOT NULL,
  timestamp TIMESTAMPTZ NOT NULL,
  fields JSONB
);
CREATE INDEX idxhash_id ON metricvalues (hash_id, timestamp DESC);


CREATE TABLE metricvaluesv2 (
  hash_id BIGINT NOT NULL,
  timestamp TIMESTAMPTZ NOT NULL,
  key VARCHAR(100) NOT NULL,
  value REAL NOT NULL
);
CREATE INDEX idxhash_id ON metricvaluesv2 (timestamp DESC, key, hash_id);

CREATE VIEW metrics AS
  SELECT
    timestamp,
    name,
    tags,
    fields
  FROM
    metricseries s
  JOIN 
    metricvalues v ON s.hash_id = v.hash_id;


CREATE OR REPLACE FUNCTION h_bigint(data TEXT) RETURNS BIGINT AS $$
 SELECT ('x'||substr(md5(data),1,16))::bit(64)::bigint;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION h_int(data TEXT) RETURNS INT AS $$
 SELECT ('x'||substr(md5(data),1,8))::bit(32)::int;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION metrics_save(data json) RETURNS void AS $$
  INSERT INTO metricseries
    SELECT DISTINCT hash_id, name, tags FROM json_to_recordset(data) AS (hash_id BIGINT, name TEXT, tags JSONB)
  ON CONFLICT DO NOTHING;

  INSERT INTO metricvalues
    SELECT * FROM json_to_recordset(data) AS (hash_id BIGINT, timestamp TIMESTAMP, fields JSONB)
  ON CONFLICT DO NOTHING;

  INSERT INTO metricvaluesv2
    SELECT hash_id,timestamp,field.key as key, (field.value)::text::real AS value
    FROM json_to_recordset(data) AS (hash_id BIGINT, timestamp TIMESTAMP, fields JSONB), jsonb_each(fields) AS field
    WHERE jsonb_typeof(field.value) = 'number';
$$ LANGUAGE SQL;

SELECT h_int(concat(name,tags,field.key)) as hash_id,name,tags,timestamp, (field.value)::text::real AS value 
FROM json_to_recordset(:'data') AS (hash_id BIGINT, name TEXT, tags JSONB, timestamp TIMESTAMP, fields JSONB), jsonb_each(fields) AS field WHERE jsonb_typeof(field.value) = 'number' limit 100;

SELECT
  floor(extract(epoch from timestamp)/60)*60 AS "time",
  tags->>'container_name' as label,
  max(values)
FROM
  metricseries s
JOIN 
  metricvaluesv2 v ON s.hash_id = v.hash_id
WHERE
  name = 'docker_container_cpu' and
  tags ? 'com_docker_compose_project' and tags->>'com_docker_compose_project' in ('common','postgres','timescale') and
  tags ? 'com_docker_compose_service' and tags->>'com_docker_compose_service' in ('node') and
  timestamp BETWEEN '2019-06-22T21:10:43Z' AND '2019-06-24T21:40:43Z' AND
  key = 'usage_percent'
group by 1,2
order by 1,2

