'use strict'

const pg = require('pg')
const debug = require('debug')

/**
 * Factory Class
 */
class Factory {
  /**
   *
   * @param {string} name
   * @returns {Promise<string>}
   */
  getProperty (name) {
    return process.env[name]
  }

  /**
   *
   * @returns {Promise<string>}
   */
  getEnvironment () {
    return this.getProperty('NODE_ENV') || 'default'
  }

  /**
   *
   * @returns {Promise<string>}
   */
  getDbConfig () {
    return this.getProperty('PG_CONNECTION')
  }

  getDBConnection () {
    const connectionString = this.getDbConfig()
    return new pg.Pool({ connectionString })
  }

  /**
   *
   * @returns {Promise<number>}
   */
  getHttpServerPort () {
    return parseInt(this.getProperty('PORT') || '3000')
  }

  getLogger () {
    return debug('MetricsPg')
  }

  getMapperHandler () {
    const crypto = require('crypto')
    const hashCode = (str) => crypto.createHash('md5').update(str).digest().readUInt32BE()

    return (req, res, next) => {
      req.metrics = (req.body.metrics || [req.body])
      .map(({timestamp, name, tags, fields}) => {
        const hash_id = hashCode(`${name}${JSON.stringify(tags)}`)
  
        return {
          timestamp: new Date(timestamp || Date.now()),
          hash_id,
          name,
          tags: Object.keys(tags)
            .map(k => [k.replace(/[\-\.]/g, '_'),tags[k]])
            .reduce((p,a) => {p[a[0]] = a[1]; return p}, {}),
          fields
        }
      })

      next()
    }
  }

  getPgDestinationHandler () {
    const conn = this.getDBConnection()
    const logger = this.getLogger()
    const sqlSeries = `
      INSERT INTO metricseries ("hash_id", "name", "tags")
      VALUES ($1, $2, $3) ON CONFLICT DO NOTHING;
    `;
    const sqlValues = `
      INSERT INTO metricvalues ("hash_id", "timestamp", "fields")
      VALUES ($1, $2, $3) ON CONFLICT DO NOTHING;
    `;
    const sqlCrate = `
    INSERT INTO metrics (hash_id, timestamp, name, tags, fields)
    VALUES (?, ?, ?, ?, ?) ON CONFLICT (timestamp, hash_id, day) DO UPDATE SET fields = excluded.fields
    `;

    return (req, res, next) => {
      Promise.all(req.metrics
      .map(data => {
        const { hash_id, timestamp, name, tags, fields} = data

        return conn.query(sqlSeries, [hash_id, name, tags])
        .then(() => ({hash_id, timestamp, fields}))
        .then(() => conn.query(sqlValues, [hash_id, timestamp, fields]))
        .then(() => null)
      }))
      .then(() => next())
      .catch(next)
    }
  }

  getPgMetricsSaveHandler () {
    const conn   = this.getDBConnection()
    const logger = this.getLogger()
    const sql    = 'select metrics_save($1)';

    return (req, res, next) => {
      conn.query(sql, [JSON.stringify(req.metrics)])
      .then(() => next())
      .catch(next)
    }
  }

  getResponseHandler() {
    const logger = this.getLogger()

    return (req, res) => {
      res.json({})
    }
  }

  getErrorHandler() {
    const logger = this.getLogger()

    return (err, req, res, next) => {
      logger(err.stack)
      next(err)
    }
  }

  getApp () {
    const port = this.getHttpServerPort()
    const logger = this.getLogger()
    const express = require('express')
    const bodyParser = require('body-parser')

    return Promise
    .resolve(express())
    .then((app) => {
      app.use(bodyParser.json({limit: '50mb'}))
      app.use(this.getMapperHandler())
      app.use(this.getPgMetricsSaveHandler())
      app.use(this.getResponseHandler())
      app.use(this.getErrorHandler())

      return app
    })
    .then((app) => new Promise((resolve, reject) => {
      app.listen(port, (err) => {
        if (err) {
          reject(err)
        } else {
          logger(`server is listening on ${port}`)
          resolve(app)
        }
      })
    }))
  }
}

module.exports = new Factory()
