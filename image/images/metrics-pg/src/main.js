'use strict'

function main () {
  const factory = require('./factories')
  const logger = factory.getLogger()

  factory.getApp().catch(logger)
}

main()
