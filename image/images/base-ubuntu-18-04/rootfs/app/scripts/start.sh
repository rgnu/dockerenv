#!/bin/bash

DIR=$(dirname $(dirname $0))
[ -f /sys/fs/cgroup/memory/memory.limit_in_bytes ] && \
MEMORY=$(($(cat /sys/fs/cgroup/memory/memory.limit_in_bytes)/1024/1048))

CMD=${CMD:-tail -f /dev/null}

cd $DIR

echo "Start ${CMD} ${CMD_OPTS} ${CMD_ARGS}"

exec ${CMD} ${CMD_OPTS} ${CMD_ARGS}
