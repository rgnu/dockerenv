'use strict'

const _ = require('highland')
const loghose = require('docker-loghose')
const Influx = require('influx')
const debug = require('debug')

/**
 * Factory Class
 */
class Factory {
  /**
   *
   * @param {string} name
   * @returns {Promise<string>}
   */
  getProperty (name) {
    return process.env[name]
  }

  /**
   *
   * @returns {Promise<string>}
   */
  getEnvironment () {
    return this.getProperty('NODE_ENV') || 'default'
  }

  /**
   *
   * @returns {Promise<string>}
   */
  getInfluxConfig () {
    return { uri: this.getProperty('INFLUX_URI') }
  }

  /**
   * @returns {InfluxDB}
   */
  getInflux () {
    const config = this.getInfluxConfig()
    return new Influx.InfluxDB(config.uri)
  }

  getLogStreamConfig () {
    const opts    = {
      includeCurrentContainer: false,
      newline: true,
      json: true
    }

    return opts
  }

  getLogger () {
    return debug('DockerLogs')
  }

  getLogStream () {
    return _(loghose(this.getLogStreamConfig()))
  }

  getMapperStream () {
    return _.pipeline(
      _.map((data) => {
        return { 
          measurement: 'logs', 
          tags: { id: data.id, image: data.image, name: data.name}, 
          fields: { message: data.line },
          timestamp: new Date(data.time)
        }
      }),
      _.batchWithTimeOrCount(60000, 100)
    )
  }

  getDestinationStream () {
    const influxClient = this.getInflux()
    const logger = this.getLogger()

    return _.pipeline(
      _.map((data) => {
        return influxClient.writePoints(
          data,
          { database: 'metrics', precision: 'ms' }
        )
        .catch(logger)
        .then(() => data)
      }),
      _.flatMap(_)      
    )
  }
}

module.exports = new Factory()
