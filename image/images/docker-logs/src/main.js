'use strict'

function main () {
  const factory = require('./factories')

  factory
  .getLogStream()
  .pipe(factory.getMapperStream())
  .pipe(factory.getDestinationStream())
  .each(factory.getLogger())
}

main()
