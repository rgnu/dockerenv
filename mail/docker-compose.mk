ENVIRONMENTS+=mail

dockerenv.mail.up: dockerenv.common.up dockerenv.image.mailcatcher.build
dockerenv.mail.%.up: dockerenv.common.up dockerenv.image.mailcatcher.build
