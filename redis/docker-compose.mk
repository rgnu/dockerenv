ENVIRONMENTS+=redis

dockerenv.redis.up: dockerenv.common.up dockerenv.image.redis.build
dockerenv.redis.%.up: dockerenv.common.up dockerenv.image.redis.build
