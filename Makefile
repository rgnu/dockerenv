DESCRIPTION=
ENVIRONMENTS=
all: list.task

rwildcard=$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))

# Include general config
include $(wildcard $(CURDIR)/tasks/config/*.mk)
include $(wildcard $(CURDIR)/tasks/*.mk)

DESCRIPTION+="list.task: Available Tasks"
list.task:
	@$(ECHO) "Tasks List: (APP_ENVIRONMENT=$(APP_ENVIRONMENT))" \
	&& (for TASK in $(DESCRIPTION); do echo "-" "$$TASK"; done) \
	| sort -t: --key=1,1

.PHONY: alias
alias:
	(for TASK in $(DESCRIPTION); do echo $$TASK; done) \
	| sed 's/://' \
	| awk '/dockerenv./{print "alias "$$1"=\"make -C $(CURDIR) "$$1"\";"}'
