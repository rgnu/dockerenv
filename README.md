# Docker Evironment
Environment to run Common Dockers used by others environment. Can be used as a developer environment.

## Dependencies
* [Docker](https://docs.docker.com/engine/installation/)
* [Docker compose](https://docs.docker.com/compose/install/)

## Tasks List

To show tasks list avaible:

```
$ cd dockerenv

$ make
```

## Test it works

```
$ make dockerenv.whoami.up
$ curl -HHost:whoami.loc http://127.0.0.1
$ curl -HHost:whoami.loc -k https://127.0.0.1
```
