ENVIRONMENTS+=postgres

dockerenv.postgres.deps: dockerenv.common.up

dockerenv.postgres.up: dockerenv.postgres.deps
dockerenv.postgres.%.up: dockerenv.postgres.deps

dockerenv.postgres.%.shell: CMD=psql postgres postgres
