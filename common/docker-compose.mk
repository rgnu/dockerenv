ENVIRONMENTS+=common

dockerenv.common.deps: dockerenv.image.docker-logs.build dockerenv.image.metrics-pg.build

dockerenv.common.up: dockerenv.common.deps
dockerenv.common.%.up: dockerenv.common.deps
