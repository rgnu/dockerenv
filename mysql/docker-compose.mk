ENVIRONMENTS+=mysql

dockerenv.mysql.%.shell: CMD=mysql -uroot

DESCRIPTION+="dockerenv.mysql.master.slave-hosts: Show Mysql Slave Hosts"
dockerenv.mysql.master.slave-hosts: CMD=mysql -uroot -e 'show slave hosts;'
dockerenv.mysql.master.slave-hosts: dockerenv.mysql.master.exec

DESCRIPTION+="dockerenv.mysql.master.status: Show Mysql Master Status"
dockerenv.mysql.master.status: CMD=mysql -uroot -e 'show master status\G'
dockerenv.mysql.master.status: dockerenv.mysql.master.exec

DESCRIPTION+="dockerenv.mysql.slave.status: Show Mysql Slave Status"
dockerenv.mysql.slave.status: CMD=mysql -uroot -e 'show slave status\G'
dockerenv.mysql.slave.status: dockerenv.mysql.slave.exec

DESCRIPTION+="dockerenv.mysql.master.sql.import SQL_FILE=*FILE*: Import SQL"
dockerenv.mysql.master.sql.import: OPTS=-T
dockerenv.mysql.master.sql.import: CMD=mysql -uroot < $(SQL_FILE)
dockerenv.mysql.master.sql.import: dockerenv.mysql.master.exec

DESCRIPTION+="dockerenv.mysql.master.sql.execute SQL=*FILE*: Run a SQL"
dockerenv.mysql.master.sql.execute: OPTS=-T
dockerenv.mysql.master.sql.execute: CMD=mysql -uroot -e "$(SQL)"
dockerenv.mysql.master.sql.execute: dockerenv.mysql.master.exec

dockerenv.mysql.up: dockerenv.common.up dockerenv.image.mysql-5-6.build
dockerenv.mysql.%.up: dockerenv.common.up dockerenv.image.mysql-5-6.build
