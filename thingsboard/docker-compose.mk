ENVIRONMENTS+=thingsboard

dockerenv.thingsboard.%: export ADD_SCHEMA_AND_SYSTEM_DATA=true
dockerenv.thingsboard.%: export ADD_DEMO_DATA=false

dockerenv.thingsboard.up: dockerenv.common.up
dockerenv.thingsboard.%.up: dockerenv.common.up
