ENVIRONMENTS+=kafka

dockerenv.kafka.deps: dockerenv.common.up dockerenv.image.zookeeper.build dockerenv.image.kafka-0-10.build
#	$(NOP)

dockerenv.kafka.up: dockerenv.kafka.deps
dockerenv.kafka.%.up: dockerenv.kafka.deps