VERBOSEOUT?=>> /dev/null

.PHONY: docker.login
DESCRIPTION+="docker.login: Login to a repository"
docker.login: OPTS+=-e='.'
docker.login:
	@$(call ASSERT_EXISTS,$(TO),TO is not defined) \
	$(call ASSERT_EXISTS,$(USER),USER is not defined) \
	$(DOCKER) login $(OPTS) \
		$(addprefix -u ,$(USER)) \
		$(addprefix -p ,$(PASSWORD)) \
		$(TO) \
	&& ($(call LOG_INFO,Login into $(TO))) \
	|| ($(call LOG_ERROR_AND_EXIT,Login into $(TO)))

.PHONY: docker.images
DESCRIPTION+="docker.images: List Images"
docker.images:
	@$(DOCKER) images

.PHONY: docker.image.pull.%
DESCRIPTION+="docker.image.pull.<NAME> <IMAGE> [OPTS]: Pull a Image"
docker.image.pull.%:
	@$(call ASSERT_EXISTS,$(IMAGE),IMAGE is not defined) \
	$(DOCKER) pull $(OPTS) $(IMAGE) $(VERBOSEOUT) \
	&& ($(call LOG_INFO,Image $(IMAGE) Pulled)) \
	|| ($(call LOG_ERROR_AND_EXIT,Pulling Image $(IMAGE)))

.PHONY: docker.image.push.%
DESCRIPTION+="docker.image.push.<NAME> <IMAGE> <TO> [OPTS]: Push a Image to a repository"
docker.image.push.%: docker.image.tag.%
	@$(call ASSERT_EXISTS,$(IMAGE),IMAGE is not defined) \
	$(call ASSERT_EXISTS,$(TO),TO is not defined) \
	$(DOCKER) push $(OPTS) $(TO) $(VERBOSEOUT) \
	&& ($(call LOG_INFO,Image $(IMAGE) Pushed to $(TO))) \
	|| ($(call LOG_ERROR_AND_EXIT,Pushing Image $(IMAGE) to $(TO)))

.PHONY: docker.image.build.%
DESCRIPTION+="docker.image.build.<NAME> <IMAGE> <SOURCE> [OPTS]: Create a Image"
docker.image.build.%:
	@$(call ASSERT_EXISTS,$(IMAGE),IMAGE is not defined) \
	$(call ASSERT_EXISTS,$(SOURCE),SOURCE is not defined) \
	$(DOCKER) build -t $(IMAGE) $(OPTS) $(SOURCE) $(VERBOSEOUT) \
	&& ($(call LOG_INFO,Image $(IMAGE) Builded)) \
	|| ($(call LOG_ERROR_AND_EXIT,Building Image $(IMAGE)))

.PHONY: docker.image.remove.%
DESCRIPTION+="docker.image.remove.<NAME> <IMAGE>: Remove a Image"
docker.image.remove.%:
	@$(call ASSERT_EXISTS,$(IMAGE),IMAGE is not defined) \
	$(DOCKER) rmi $(IMAGE) $(VERBOSEOUT) \
	&& ($(call LOG_INFO,Image $(IMAGE) Removed)) \
	|| ($(call LOG_ERROR_AND_EXIT,Removing Image $(IMAGE)))

.PHONY: docker.image.tag.%
DESCRIPTION+="docker.image.tag.<NAME> <IMAGE> <TO> [OPTS]: Tag a Image"
docker.image.tag.%:
	@$(call ASSERT_EXISTS,$(IMAGE),IMAGE is not defined) \
	$(call ASSERT_EXISTS,$(TO),TO is not defined) \
	$(DOCKER) tag $(OPTS) $(IMAGE) $(TO) $(VERBOSEOUT) \
	&& ($(call LOG_INFO,Image $(IMAGE) Tagged to $(TO))) \
	|| ($(call LOG_ERROR_AND_EXIT,Tagging Image $(IMAGE)))




.PHONY: docker.containers
DESCRIPTION+="docker.containers [OPTS]: List Containers"
docker.containers:
	@$(DOCKER) ps $(OPTS)

.PHONY: docker.containers.names
DESCRIPTION+="docker.containers.names [OPTS]: List Containers names"
docker.containers.names:
	@$(DOCKER) ps $(OPTS) | awk '{print $$NF}' | tail -n +2


.PHONY: docker.container.create.%
DESCRIPTION+="docker.container.create.<NAME> <IMAGE> [OPTS] [CMD]: Create a Container"
docker.container.create.%:
	@$(call ASSERT_EXISTS,$(IMAGE),IMAGE is not defined) \
	$(call DOCKER_CONTAINER_EXISTS,$*) \
	&& ($(call LOG_WARN,Container $* exists)) \
	|| ($(DOCKER) create --name $* $(OPTS) \
            $(addprefix --env ,$(ENV)) \
            $(addprefix -p ,$(PORTS)) \
            $(addprefix --link ,$(LINKS)) \
            $(addprefix --volume ,$(VOLUMES)) \
            $(addprefix --volumes-from ,$(VOLUMES_FROM)) \
            $(IMAGE) $(CMD) $(VERBOSEOUT) \
		&& ($(call LOG_INFO,Container $* Created)) \
		|| ($(call LOG_ERROR_AND_EXIT,Creating Container $*)) \
    )


.PHONY: docker.container.remove-and-create.%
DESCRIPTION+="docker.container.remove-and-create.<NAME> <IMAGE> [OPTS] [CMD]: Create a Container"
docker.container.remove-and-create.%:  docker.container.remove.% docker.container.create.%
	$(NOP)

.PHONY: docker.container.info.%
DESCRIPTION+="docker.container.info.<NAME> [OPTS]: Return info about a container"
docker.container.info.%:
	@$(DOCKER) inspect $(OPTS) $*


.PHONY: docker.container.logs.%
DESCRIPTION+="docker.container.logs.<NAME> [OPTS]: See container logs"
docker.container.logs.%:
	@$(DOCKER) logs $(OPTS) $*; exit 0;


.PHONY: docker.container.exists.%
DESCRIPTION+="docker.container.exists.<NAME>: Test if a container exists"
DOCKER_CONTAINER_EXISTS=$(DOCKER) ps --all=true | awk '{print $$NF}' | grep -q '^$(1)$$'
docker.container.exists.%:
	@$(call DOCKER_CONTAINER_EXISTS,$*);


.PHONY: docker.container.is_running.%
DESCRIPTION+="docker.container.is_running.<NAME>: Test if a container is running"
DOCKER_CONTAINER_IS_RUNNING=$(DOCKER) ps | awk '{print $$NF}' | grep -q '^$(1)$$'
docker.container.is_running.%:
	@$(call DOCKER_CONTAINER_IS_RUNNING,$*);


.PHONY: docker.container.exec.%
DESCRIPTION+="docker.container.exec.<NAME> [OPTS] <CMD>: Exec a command into a running container"
docker.container.exec.%:
	@$(call ASSERT_EXISTS,$(CMD),CMD is not defined) \
	$(DOCKER) exec $(OPTS) $* $(CMD)


.PHONY: docker.container.start.%
DESCRIPTION+="docker.container.start.<NAME>: Start a container"
docker.container.start.%:
	@$(DOCKER) start $* $(VERBOSEOUT) \
	&& ($(call LOG_INFO,Container $* Started)) \
	|| ($(call LOG_ERROR_AND_EXIT,Starting Container $*))


.PHONY: docker.container.stop.%
DESCRIPTION+="docker.container.stop.<NAME> [TIME=10]: Stop a container"
docker.container.stop.%: TIME?=10
docker.container.stop.%:
	@$(call DOCKER_CONTAINER_IS_RUNNING,$*) \
	&& ($(DOCKER) stop --time=$(TIME) $* $(VERBOSEOUT) \
		&& ($(call LOG_INFO,Container $* Stopped))) \
	|| ($(call LOG_WARN,Container $* is not running))


.PHONY: docker.container.kill.%
DESCRIPTION+="docker.container.kill.<NAME> [SIGNAL=KILL]: Kill a container"
docker.container.kill.%: SIGNAL?=KILL
docker.container.kill.%:
	@$(DOCKER) kill --signal="$(SIGNAL)" $*


.PHONY: docker.container.wait.%
DESCRIPTION+="docker.container.wait.<NAME>: Wait a container"
docker.container.wait.%:
	@$(DOCKER) wait $*


.PHONY: docker.container.remove.%
DESCRIPTION+="docker.container.remove.<NAME>: Remove a Container"
docker.container.remove.%: docker.container.stop.%
	@$(call DOCKER_CONTAINER_EXISTS,$*) \
	&& ($(DOCKER) rm $* $(VERBOSEOUT) \
		&& ($(call LOG_INFO,Container $* Removed)) \
		|| ($(call LOG_ERROR,Removing Container $*))) \
	|| ($(call LOG_WARN,Container $* does not exists))

.PHONY: docker.container.stopall
DESCRIPTION+="docker.container.stopall: stops all containers"
docker.container.stopall:
	$(MAKE) docker.containers.names | xargs -n1 -P6 -I {F} $(MAKE) docker.container.stop.{F}
