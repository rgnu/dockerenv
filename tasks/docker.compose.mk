docker.compose.%: DOCKER_COMPOSE?=$(CURDIR)/docker-compose $(addprefix -f ,$(FILE))
docker.compose.%: GET_NS=$(word 1,$(subst ., ,$(1)))
docker.compose.%: GET_NAME=$(word 2,$(subst ., ,$(1)))
docker.compose.%: ENVIRONMENT?=$(or $(call GET_NS,$*),common)
docker.compose.%: SERVICE?=$(call GET_NAME,$*)


docker.compose.test.%:
	echo "ENV=$(ENVIRONMENT) SVC=$(SERVICE)"

.PHONY: docker.compose.build.%
DESCRIPTION+="docker.compose.build.<ENVIRONMENT>.[SERVICE]: Build service images"
docker.compose.build.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(DOCKER_COMPOSE) build $(OPTS) $(SERVICE) \
	&& ($(call LOG_INFO,Builded $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Building $(ENVIRONMENT) $(SERVICE)))

.PHONY: docker.compose.pull.%
DESCRIPTION+="docker.compose.pull.<ENVIRONMENT>.[SERVICE]: Pulling service images"
docker.compose.pull.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(DOCKER_COMPOSE) pull $(OPTS) $(SERVICE) \
	&& ($(call LOG_INFO,Pulled $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Pulling $(ENVIRONMENT) $(SERVICE)))


.PHONY: docker.compose.up.%
DESCRIPTION+="docker.compose.up.<ENVIRONMENT>.[SERVICE]: Create and start containers"
docker.compose.up.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(DOCKER_COMPOSE) up $(OPTS) $(SERVICE) \
	&& ($(call LOG_INFO,Up and running $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Building/Creating $(ENVIRONMENT) $(SERVICE)))


.PHONY: docker.compose.down.%
DESCRIPTION+="docker.compose.down.<ENVIRONMENT>.[SERVICE]: Stop and Remove all reference (containers,networks,volume,etc)"
docker.compose.down.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(DOCKER_COMPOSE) down $(OPTS) $(SERVICE) \
	&& ($(call LOG_INFO,Stopped and removed $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Stopping/Removing $(ENVIRONMENT) $(SERVICE)))


.PHONY: docker.compose.start.%
DESCRIPTION+="docker.compose.start.<ENVIRONMENT>.[SERVICE]: Start containers"
docker.compose.start.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(DOCKER_COMPOSE) start $(OPTS) $(SERVICE) \
	&& ($(call LOG_INFO,Started $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Starting $(ENVIRONMENT) $(SERVICE)))


.PHONY: docker.compose.stop.%
DESCRIPTION+="docker.compose.stop.<ENVIRONMENT>.[SERVICE]: Stop containers"
docker.compose.stop.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(DOCKER_COMPOSE) stop $(OPTS) $(SERVICE) \
	&& ($(call LOG_INFO,Stopped $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Stopping $(ENVIRONMENT) $(SERVICE)))


.PHONY: docker.compose.restart.%
DESCRIPTION+="docker.compose.restart.<ENVIRONMENT>.[SERVICE]: Restart containers"
docker.compose.restart.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(DOCKER_COMPOSE) restart $(OPTS) $(SERVICE) \
	&& ($(call LOG_INFO,Restarted $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Restarting $(ENVIRONMENT) $(SERVICE)))


.PHONY: docker.compose.rm.%
DESCRIPTION+="docker.compose.rm.<ENVIRONMENT>.[SERVICE]: Remove stopped containers"
docker.compose.rm.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(DOCKER_COMPOSE) rm $(OPTS) $(SERVICE) \
	&& ($(call LOG_INFO,Removed $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Removing $(ENVIRONMENT) $(SERVICE)))


.PHONY: docker.compose.logs.%
DESCRIPTION+="docker.compose.logs.<ENVIRONMENT>.[SERVICE]: View output from containers"
docker.compose.logs.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(DOCKER_COMPOSE) logs $(OPTS) $(SERVICE) \
	&& ($(call LOG_INFO,Logs from $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Logs $(ENVIRONMENT) $(SERVICE)))

.PHONY: docker.compose.ps.%
DESCRIPTION+="docker.compose.ps.<ENVIRONMENT>: View output from containers"
docker.compose.ps.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(DOCKER_COMPOSE) ps $(OPTS) \
	&& ($(call LOG_INFO,Status from $(ENVIRONMENT))) \
	|| ($(call LOG_ERROR_AND_EXIT,Status from $(ENVIRONMENT)))


.PHONY: docker.compose.exec.%
DESCRIPTION+="docker.compose.exec.<ENVIRONMENT>.<SERVICE>: Exec a command into a running container"
docker.compose.exec.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(call ASSERT_EXISTS,$(SERVICE),SERVICE is not defined) \
	$(DOCKER_COMPOSE) exec $(OPTS) $(SERVICE) $(CMD) \
	&& ($(call LOG_INFO,Exec from $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Exec from $(ENVIRONMENT) $(SERVICE)))

.PHONY: docker.compose.run.%
DESCRIPTION+="docker.compose.run.<ENVIRONMENT>.<SERVICE>: Run a command into a container"
docker.compose.run.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(call ASSERT_EXISTS,$(SERVICE),SERVICE is not defined) \
	$(DOCKER_COMPOSE) run $(OPTS) $(SERVICE) $(CMD) \
	&& ($(call LOG_INFO,Run from $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Run from $(ENVIRONMENT) $(SERVICE)))


.PHONY: docker.compose.scale.%
DESCRIPTION+="docker.compose.scale.<ENVIRONMENT>.<SERVICE> REPLICAS=<REPLICAS>: Scale a service"
docker.compose.scale.%:
	@$(call ASSERT_EXISTS,$(FILE),FILE is not defined) \
	$(call ASSERT_EXISTS,$(SERVICE),SERVICE is not defined) \
	$(call ASSERT_EXISTS,$(REPLICAS),REPLICAS is not defined) \
	$(DOCKER_COMPOSE) scale $(OPTS) $(SERVICE)=$(REPLICAS) \
	&& ($(call LOG_INFO,Exec from $(ENVIRONMENT) $(SERVICE))) \
	|| ($(call LOG_ERROR_AND_EXIT,Exec from $(ENVIRONMENT) $(SERVICE)))


.PHONY: docker.compose.%
docker.compose.%: docker-compose
	$(NOP)

DESCRIPTION+="docker-compose: Download Docker Compose"
docker-compose:
	($(call LOG_INFO,Downloading $@)) \
	&& $(CURL) -L -s https://github.com/docker/compose/releases/download/$(DOCKER_COMPOSE_VERSION)/docker-compose-`uname -s`-`uname -m` > $(CURDIR)/docker-compose \
	&& chmod +x $(CURDIR)/docker-compose \
	&& ($(call LOG_INFO,Downloaded $@)) \
	|| ($(call LOG_ERROR_AND_EXIT,Downloading $@))

DESCRIPTION+="docker-machine: Download Docker Machine"
docker-machine:
	($(call LOG_INFO,Downloading $@)) \
	$(CURL) -L -s https://github.com/docker/machine/releases/download/v0.7.0/docker-machine-`uname -s`-`uname -m` > $(CURDIR)/docker-machine \
	&& chmod +x $(CURDIR)/docker-machine \
	&& ($(call LOG_INFO,Downloaded $@)) \
	|| ($(call LOG_ERROR_AND_EXIT,Downloading $@))
