.SILENT:

APP_ENVIRONMENT?=production
APP_DIR?=$(CURDIR)
APP_BIN_DIR?=$(APP_DIR)/bin
APP_BUILD_DIR?=$(APP_DIR)/build
APP_TASK_DIR?=$(APP_DIR)/tasks
WGET?=/usr/bin/wget
DOCKER?=docker
BOOT2DOCKER?=boot2docker
ECHO?=echo
NOP?=$(ECHO) >> /dev/null
CURL?=curl
OS?=$(shell uname)
DOCKER_COMPOSE_VERSION=1.21.2

include $(wildcard $(APP_TASK_DIR)/config/$(APP_ENVIRONMENT)/*.mk)

#VERBOSEOUT?=| awk '{printf(".");system("")} END{print ""}'

VERBOSEOUT=>> /dev/stdout

ifeq ($(OS),Linux)
	USER_ID?=$(shell id -u)
endif

USER_ID?=1000
