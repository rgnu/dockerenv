
db.migrate.%: VERSION_FILE?=$(DIR)/$(APP_ENVIRONMENT).version

.PHONY: db.migrate.%
DESCRIPTION+="db.migrate.<NAME>: Do DB migration task"
db.migrate.%:
	$(call ASSERT_EXISTS,$(DIR),DIR is not defined) \
	$(call ASSERT_EXISTS,$(COMMAND),COMMAND is not defined) \
	$(call LOG_INFO,$@: Start); \
	[ -f $(VERSION_FILE) ] && . $(VERSION_FILE) || VERSION=0; RESULT="0"; \
	for FILE in $$(ls -X1 $(DIR)/*.sql ); do \
		FILE_VERSION=$$(basename $$FILE | cut -d"_" -f1); \
		if [ $$FILE_VERSION -gt $$VERSION -a "$$RESULT" -eq "0" ]; then \
			MSG="Update DB with $$FILE revision $$FILE_VERSION"; \
			OUTPUT=$$($(COMMAND) 2>&1); \
			RESULT=$$?; \
			[ "$$RESULT" -eq "0" ] \
				&& $(ECHO) "VERSION=$$FILE_VERSION" > $(VERSION_FILE) \
				&& $(call LOG_INFO,$@: $$MSG) \
				|| $(call LOG_ERROR,$@: $$MSG \n\texit with $$RESULT ($$OUTPUT)); \
		fi; \
	done; \
	$(call LOG_INFO,$@: End); \
	exit $$RESULT
