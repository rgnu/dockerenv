# Global Config
dockerenv.%: FILE=$(CURDIR)/$(firstword $(subst ., ,$*))/docker-compose.yml
dockerenv.%.up: OPTS=-d
dockerenv.%.logs: OPTS=-f --tail 10
dockerenv.%.build: OPTS=--force-rm

# Global Tasks
DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).up")
.PHONY: dockerenv.%.up
dockerenv.%.up: docker.compose.up.%
	$(NOP)

DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).down")
.PHONY: dockerenv.%.down
dockerenv.%.down: docker.compose.down.%
	$(NOP)


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).build")
.PHONY: dockerenv.%.build
dockerenv.%.build: docker.compose.build.%
	touch $@


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).pull")
.PHONY: dockerenv.%.pull
dockerenv.%.pull: docker.compose.pull.%
	$(NOP)


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).start")
.PHONY: dockerenv.%.start
dockerenv.%.start: docker.compose.start.%
	$(NOP)


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).stop")
.PHONY: dockerenv.%.stop
dockerenv.%.stop: docker.compose.stop.%
	$(NOP)

DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).restart")
.PHONY: dockerenv.%.restart
dockerenv.%.restart: docker.compose.restart.%
	$(NOP)


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).rm")
.PHONY: dockerenv.%.rm
dockerenv.%.rm: docker.compose.rm.%
	$(NOP)

DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).rmf")
.PHONY: dockerenv.%.rmf
dockerenv.%.rmf: OPTS=-f
dockerenv.%.rmf: docker.compose.rm.%
	$(NOP)


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).logs")
.PHONY: dockerenv.%.logs
dockerenv.%.logs: docker.compose.logs.%
	$(NOP)


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).ps")
.PHONY: dockerenv.%.ps
dockerenv.%.ps: docker.compose.ps.%
	$(NOP)

DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).SERVICE.exec CMD=*COMMAND*")
.PHONY: dockerenv.%.exec
dockerenv.%.exec: docker.compose.exec.%
	$(NOP)


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).SERVICE.run CMD=*COMMAND*")
.PHONY: dockerenv.%.run
dockerenv.%.run: OPTS=--rm
dockerenv.%.run: docker.compose.run.%
	$(NOP)


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).SERVICE.scale REPLICAS=*NUMBER*")
.PHONY: dockerenv.%.scale
dockerenv.%.scale: docker.compose.scale.%
	$(NOP)


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).SERVICE.shell")
.PHONY: dockerenv.%.shell
dockerenv.%.shell: CMD=bash
dockerenv.%.shell: docker.compose.exec.%
	$(NOP)


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).SERVICE.ip: Get Service IP")
.PHONY: dockerenv.%.shell
dockerenv.%.ip: CMD=hostname -i
dockerenv.%.ip: docker.compose.exec.%
	$(NOP)


DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).dashboards.upload")
.PHONY: dockerenv.%.dashboards.upload
dockerenv.%.dashboards.upload: FROM=$(CURDIR)/$*/dashboards/
dockerenv.%.dashboards.upload: TO=http://localhost
dockerenv.%.dashboards.upload:
	@$(call ASSERT_EXISTS,$(TO),TO is not defined) \
	$(call ASSERT_EXISTS,$(FROM),FROM is not defined) \
	$(foreach name,$(notdir $(wildcard $(FROM)/*)), \
		curl -s -HContent-type:application/json -HHost:grafana.dev -u admin:admin -d@$(FROM)$(name) $(TO)/api/dashboards/import > /dev/null \
		&& ($(call LOG_INFO,Uploaded Grafana Dashboard $*/$(name))) \
		|| ($(call LOG_ERROR,Uploading Grafana Dashboard $*/$(name))); \
	)

DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).SERVICE.test.unit")
.PHONY: dockerenv.%.test.unit
dockerenv.%.test.unit: dockerenv.%.test.unit.exec
	$(NOP)

DESCRIPTION+=$(foreach env,$(ENVIRONMENTS),"dockerenv.$(env).SERVICE.test.acceptance")
.PHONY: dockerenv.%.test.acceptance
dockerenv.%.test.acceptance: docker.compose.exec.%.test.acceptance
	$(NOP)

# Include Compose Tasks
include $(wildcard $(CURDIR)/*/docker-compose.mk)


.PHONY: dockerenv.all.ps
DESCRIPTION+="dockerenv.all.ps:"
dockerenv.all.ps: $(foreach env,$(ENVIRONMENTS),dockerenv.$(env).ps)

.PHONY: dockerenv.all.stop
dockerenv.all.stop: $(foreach env,$(ENVIRONMENTS),dockerenv.$(env).stop)
