ENVIRONMENTS+=hello

dockerenv.hello.deps: dockerenv.image.vertx-hello

dockerenv.hello.up: dockerenv.hello.deps
dockerenv.hello.%.up: dockerenv.hello.deps
