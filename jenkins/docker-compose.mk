ENVIRONMENTS+=jenkins

dockerenv.postgres.deps: dockerenv.common.proxy.up

dockerenv.jenkins.up: dockerenv.postgres.deps
dockerenv.jenkins.%.up: dockerenv.postgres.deps
