ENVIRONMENTS+=mongo

dockerenv.mongo.deps: dockerenv.common.up dockerenv.image.mongo-3-2.build

dockerenv.mongo.up: dockerenv.mongo.deps
dockerenv.mongo.%.up: dockerenv.mongo.deps

dockerenv.mongo.%.shell: CMD=mongo