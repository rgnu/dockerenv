CREATE VIEW metrics_values AS
  SELECT
    minute(cast(payload->>'timestamp' as timestamptz)) as ts,
    cast(payload->>'hash_id' as bigint) as hash_id,
    key,
    sum(cast(value::text as float)) as sum,
    count(cast(value::text as float)) as count,
    min(cast(value::text as float)) as min,
    max(cast(value::text as float)) as max
  FROM
    json_stream,
    json_each(cast(payload->>'fields' as json))
  GROUP BY ts, hash_id, key;

CREATE INDEX ON metrics_values (hash_id, key, ts);

CREATE VIEW metrics_series WITH (action=materialize) AS 
  SELECT
    cast(payload->>'hash_id' as bigint) as hash_id,
    payload->>'name' as name,
    cast(payload->>'tags' as jsonb) as tags
  FROM
    json_stream
  GROUP BY hash_id, name, tags;

CREATE VIEW metrics AS
  SELECT
    ts,
    name,
    tags,
    key,
    sum,
    count,
    sum/count as avg,
    min,
    max
  FROM
    metrics_values v,
    metrics_series s
  WHERE
    v.hash_id = s.hash_id;
