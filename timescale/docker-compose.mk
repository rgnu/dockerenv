ENVIRONMENTS+=timescale

dockerenv.timescale.up: dockerenv.common.up
dockerenv.timescale.%.up: dockerenv.common.up

dockerenv.timescale.%.shell: CMD=psql postgres postgres